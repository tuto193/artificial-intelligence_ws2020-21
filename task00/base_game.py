import numpy as np
from enum import IntEnum


class Action(IntEnum):
    """
    Represents the possible actions as ints.
    Note, that the order of the items should not be changed,
    as they indictate the number of rotations necessary for the sliding step.
    """

    left = 0
    up = 1
    right = 2
    down = 3


class Board2048(object):
    def __init__(self, width: int = 4, height: int = 4, seed=None):
        """
        Represents a 2048 board with width and height. Actions can be performed on the board
        with the step() function.

        Parameters
        ----------
        with: int
            The width of the board
        height: int
            The height of the board
        seed: any
            The seed for the random state. Using the same seed will result in the same game to be played
        """
        self.width: int = width
        self.height: int = height
        self.score: int = 0

        # Internal Variables
        self.board = None
        self.np_random = None

        self.seed(seed=seed)
        self.reset()

    def seed(self, seed=None):
        """
        Seed the random state.

        Parameters
        ----------
        seed: any
            The seed for the random state. Using the same seed will result in the same game to be played
        """

        self.np_random = np.random.RandomState(seed=seed)
        return [seed]

    def step(self, action: Action):
        """
        Perform one action on the board.
        Possible actions are [left, right, up, down].
        Note, that impossible actions will result in no board change.

        Parameters
        ----------
        action: Action
            The action to perform

        Returns
        -------
        board: array_like
            The new board state
        score: int
            The gained score from this move
        game_over: bool
            Is the game over?
        dict: dict
            Currently unused (but expected by the test code later!)
        """

        game_over = False
        score = 0

        # TODO: INSERT YOUR CODE HERE
        # assuming a board is accessed as in board[y][x], where :
        # [0][0] = bottom left
        # [y][x] = top right
        if action == Action.left:
            # start from the LEFT and pull everything towards it
            for h in range(self.height):
                for w in range(self.width):
                    if self.board[h, w] == 0:
                        # empty cells are unimportant
                        continue
                    # not-empty cells are pulled to the left side
                    margin = w - 1
                    while margin >= 0:
                        if self.board[h, margin] == 0:
                            if margin == 0:
                                # margin is already the left-most element
                                self.board[h, margin] = self.board[h, w]
                                self.board[h, w] = 0
                        elif self.board[h, margin] == self.board[h, w]:
                            # if the cells are equal, merge them
                            self.board[h, margin] *= 2
                            score += self.board[h, margin]
                            self.board[h, w] = 0
                            break
                        # first obstacle to the left
                        elif self.board[h, margin] != self.board[h, w]:
                            if margin + 1 != w:
                                # the obstable is not directly next to us
                                self.board[h, margin + 1] = self.board[h, w]
                                self.board[h, w] = 0
                            break
                        margin -= 1
        elif action == Action.right:
            # start from the right and pull everything towards it
            for h in range(self.height):
                for w in range(self.width - 1, -1, -1):
                    if self.board[h, w] == 0:
                        # empty cells are unimportant
                        continue
                    # not-empty cells are pulled to the left side
                    margin = w + 1
                    while margin <= self.width - 1:
                        if self.board[h, margin] == 0:
                            if margin == self.width - 1:
                                # margin is already the right-most element
                                self.board[h, margin] = self.board[h, w]
                                self.board[h, w] = 0
                        elif self.board[h, margin] == self.board[h, w]:
                            # if the cells are equal, merge them
                            self.board[h, margin] *= 2
                            score += self.board[h, margin]
                            self.board[h, w] = 0
                            break
                        # first obstable to the right
                        elif self.board[h, margin] != self.board[h, w]:
                            if margin - 1 != w:
                                # the obstable is not directly next to us
                                self.board[h, margin - 1] = self.board[h, w]
                                self.board[h, w] = 0
                            break
                        margin += 1
        elif action == Action.up:
            # start from the BOTTOM (now we're here) and pull everything
            # towards it
            for w in range(self.width):
                for h in range(self.height):
                    if self.board[h, w] == 0:
                        # empty cells are unimportant
                        continue
                    # not-empty cells are pulled to the left side
                    margin = h - 1
                    while margin >= 0:
                        if self.board[margin, w] == 0:
                            if margin == 0:
                                # margin is already the left-most element
                                self.board[margin, w] = self.board[h, w]
                                self.board[h, w] = 0
                        elif self.board[margin, w] == self.board[h, w]:
                            # if the cells are equal, merge them
                            self.board[margin, w] *= 2
                            score += self.board[margin, w]
                            self.board[h, w] = 0
                            break
                        # first obstacle below
                        elif self.board[margin, w] != self.board[h, w]:
                            if margin + 1 != w:
                                # the obstable is not directly next to us
                                self.board[margin + 1, w] = self.board[h, w]
                                self.board[h, w] = 0
                            break
                        margin -= 1
        elif action == Action.down:
            # start from the top and pull everything towards it
            for w in range(self.width):
                for h in range(self.height - 1, -1, -1):
                    if self.board[h, w] == 0:
                        # empty cells are unimportant
                        continue
                    # not-empty cells are pulled to the top
                    margin = h + 1
                    while margin <= self.height - 1:
                        if self.board[margin, w] == 0:
                            if margin == self.height - 1:
                                # margin is already the top-most element
                                self.board[margin, w] = self.board[h, w]
                                self.board[h, w] = 0
                        elif self.board[margin, w] == self.board[h, w]:
                            # if the cells are equal, merge them
                            # AND make the score their result
                            self.board[margin, w] *= 2
                            score += self.board[margin, w]
                            self.board[h, w] = 0
                            break
                        # first obstable to the right
                        elif self.board[margin, w] != self.board[h, w]:
                            if margin - 1 != w:
                                # the obstable is not directly next to us
                                self.board[margin - 1, w] = self.board[h, w]
                                self.board[h, w] = 0
                            break
                        margin += 1

        self.score += score
        self._place_random_tiles(board=self.board)
        game_over = self.game_over()
        return self.board, score, game_over, {}

    def game_over(self):
        """
        Check if the game is over.

        Returns
        -------
        game_over: bool
            True if the game is over, False otherwise
        """

        # TODO: INSERT YOUR CODE HERE
        for i in range(self.width):
            for j in range(self.height):
                if self.board[j, i] == 0:
                    # the board still has an empty space where to place a new
                    # piece
                    return False
        for i in range(self.width - 1):
            for j in range(self.height - 1):
                if self.board[j, i] == self.board[j, i + 1] or \
                        self.board[j, i] == self.board[j + 1, i]:
                    # at least another pair can be made
                    return False
        return True

    def reset(self):
        """Place 2 tiles on the empty board."""

        self.board = np.zeros((self.width, self.height), dtype=np.int64)
        self._place_random_tiles(self.board, count=2)

        return self.board

    def _sample_tiles(self, count=1):
        """Sample tile 2 or 4."""

        choices = [2, 4]
        probs = [0.9, 0.1]

        tiles = self.np_random.choice(choices, size=count, p=probs)
        return tiles.tolist()

    def _sample_tile_locations(self, board, count=1):
        """Sample grid locations with no tile."""

        zero_locs = np.argwhere(board == 0)
        zero_indices = self.np_random.choice(len(zero_locs), size=count)

        zero_pos = zero_locs[zero_indices]
        zero_pos = list(zip(*zero_pos))
        return zero_pos

    def _place_random_tiles(self, board, count=1):
        """
        Place count random tiles on the board.

        Parameters
        ----------
        board: array_like
            The board to place the tiles on
        count: int
            The number of tiles to place (default: 1)
        """
        # if not board.all():
        tiles = self._sample_tiles(count)
        tile_locs = tuple(self._sample_tile_locations(board, count))
        board[tile_locs] = tiles

        # TODO: INSERT YOUR CODE HERE

import numpy as np
import maze
import time


def manhattan_d(a: (int, int), b: (int, int)) -> int:
    """
    Measure the distance between two points a and b

    Args:
        a (int, int) : coordinates of the first point
        b (int, int) : coordinates of the second point
    Returns:
        (int) distance between both points
    """
    x = a[0] - b[0]
    if x < 0:
        x = -x
    y = a[1] - b[1]
    if y < 0:
        y = -y
    return x + y


def sort_distances(coords: list, goal: (int, int)) -> list:
    """
    Sort a list of coordinates based on their individual distances to a goal.
    (closest-farthest)

    Args:
        coords (list): list of coordinates that are to be sorted.
        goal ((int, int)): the coordinates of the goal that are to be sorted
            against
    Returns:
        (list) the sorted coordinates
    """
    distances = []
    for i in coords:
        distances.append(manhattan_d(i, goal))
    coords, distances = zip(*sorted(zip(coords, distances)))
    return list(coords)


def best_first_search(maze: maze.Maze, start, goal, plot=False):
    """
    Implementation of the best first search.

    Returns:
        list: the path from the start to the goal
        (int, int, int): a tuple composed of (in this order):
            * amount of expanded nodes,
            * maximal depth of the search tree, and
            * maximal size of the search front.
    """
    stopwatch = time.time()
    node_tree = [[start]]
    visited = [start]
    path: list = []
    expanded_nodes = 0
    max_front_size = 0
    while any(node_tree):
        chosen_depth = 0
        # Sort the distances on the expanded tree
        node_tree = list(sort_distances(x, goal) for x in node_tree)
        # Get the closest of all nodes with respect to the goal
        for i, level in enumerate(node_tree):
            if level[0] < node_tree[chosen_depth][0]:
                chosen_depth = i
        path.append(node_tree[chosen_depth][0])
        node_tree[chosen_depth] = node_tree[chosen_depth][1:]
        if path[-1] == goal:
            path = np.array(path).T
            return path, (
                time.time() - stopwatch,
                expanded_nodes,
                len(node_tree),
                max_front_size
            )
        neighbors = maze.get_neighbors(path[-1], set(visited))
        visited += neighbors
        node_tree[chosen_depth + 1] += neighbors
        actual_front = 0
        for i in node_tree:
            actual_front += len(i)
        max_front_size = (
            max_front_size if max_front_size > actual_front else actual_front
        )
        expanded_nodes += 1


def astar(maze: maze.Maze, start, goal, plot=False):
    """
    Implementation of the A* search.

    Returns:
        list: the path from the start to the goal
        (int, int, int): a tuple composed of (in this order):
            * amount of expanded nodes,
            * maximal depth of the search tree, and
            * maximal size of the search front.
    """
    stopwatch = time.time()
    node_tree = [[start]]
    visited = [start]
    path: list = []
    expanded_nodes = 0
    max_front_size = 0
    while any(node_tree):
        chosen_depth = 0
        # Sort the distances on the expanded tree
        node_tree = list(sort_distances(x, goal) for x in node_tree)
        # Get the closest of all nodes with respect to the goal
        for i, level in enumerate(node_tree):
            if level[0] < node_tree[chosen_depth][0]:
                chosen_depth = i
        path.append(node_tree[chosen_depth][0])
        node_tree[chosen_depth] = node_tree[chosen_depth][1:]
        if path[-1] == goal:
            path = np.array(path).T
            return path, (
                stopwatch - time.time(),
                expanded_nodes,
                len(node_tree),
                max_front_size
            )
        neighbors = maze.get_neighbors(path[-1], set(visited))
        visited += neighbors
        node_tree[chosen_depth + 1] += neighbors
        actual_front = 0
        for i in node_tree:
            actual_front += len(i)
        max_front_size = (
            max_front_size if max_front_size > actual_front else actual_front
        )
        expanded_nodes += 1

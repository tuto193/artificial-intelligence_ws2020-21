import json
import argparse
import uuid

parser = argparse.ArgumentParser(description="Convert a Notebook to it's solution version.")
parser.add_argument('input', type=str,
                    help='the input file')
parser.add_argument('output', type=str, nargs='?', default="",
                    help='the output file')
args = parser.parse_args()

ipy_suffix = '.ipynb'
if not args.input.endswith(ipy_suffix):
    raise FileError("Invalid file type")
if not args.output:
    args.output = args.input.replace(ipy_suffix, '_solution'+ ipy_suffix)

# load the notebook as a json file
with open(args.input) as json_file:
    notebook = json.load(json_file)

# insert disclaimer first
disclaimer = {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": False,
     "grade_id": "disclaimer",
     "locked": False,
     "schema_version": 3,
     "solution": False,
     "task": False
     }
    },
   "source": [
    "# Disclaimer\n",
    "\n",
    "Die hier enthaltenen Lösungen stellen nur Beispiele dar. Sie erheben keinerlei Anspruch auf Richtig- und Vollständigkeit und sind **nicht** zur Vorbereitung für die Klausur geeignet! \n",
    "\n",
    "Jegliche Vervielfältigung und Verbreitung über die Veranstaltung \"Künstliche Intelligenz\" im Wintersemester 2020/21 der Universität Osnabrück hinaus ist nicht gestattet."
   ]
  }

notebook["cells"].insert(0, disclaimer)

for idx, cell in enumerate(notebook["cells"]):
    print("old cell")
    print(cell)
    make_tuple = False
    if isinstance(cell, (tuple, list)):
        cell = cell[0]
        make_tuple = True

    if "nbgrader" not in cell["metadata"]:
        cell["metadata"]["nbgrader"] = {}
    meta = cell["metadata"]["nbgrader"]
    if "schema_version" not in meta:
        meta["schema_version"] = 3

    if "grade_id" not in meta:
        print("adding GRADE_ID")
        meta["grade_id"] = "cell-" + uuid.uuid4().hex[:16]

    # convert everything to a normal cell and lock it
    meta["solution"] =  False
    meta["task"] = False
    meta["locked"] = True
    meta["grade"] = False

    if "points" in meta:
        del meta["points"]

    cell["metadata"]["nbgrader"] = meta

    if cell["cell_type"] == "code":
        for i, source in enumerate(cell["source"]):
            cell["source"][i] = source.replace("### BEGIN SOLUTION", "")
            cell["source"][i] = cell["source"][i].replace("### END SOLUTION", "")
        # remove outputs
        cell["execution_count"]: 1
        cell["outputs"] =  []

    # remove mark schemes in markdown cells
    if cell["cell_type"] == "markdown":
        for i, source in enumerate(cell["source"]):
            cell["source"][i] = source.replace("=== BEGIN MARK SCHEME ===\n", "")
            cell["source"][i] = cell["source"][i].replace("=== END MARK SCHEME ===\n", "")
    
    print("----------------------------------------------------------------------------------------------")
    print("modified cell")
    print(cell)
    print("----------------------------------------------------------------------------------------------")

    if make_tuple:
        cell = (cell,)
    notebook["cells"][idx] = cell


    
with open(args.output, 'w') as outfile:
    json.dump(notebook, outfile)

from isolated_prolog import IsolatedProlog # use the improved version of the Prolog object

prolog_code = """

/******************************* TAGE *****************************/
wochentag('27.11.2020',freitag).
wochentag('30.11.2020',montag).
wochentag('01.12.2020',dienstag).
wochentag('02.12.2020',mittwoch).
wochentag('03.12.2020',donnerstag).
wochentag('04.12.2020',freitag).

/******************************* GERICHTE *****************************/
gericht(kabeljau,fisch).
gericht(dorade,fisch).
gericht(lachs,fisch).
gericht(forelle,fisch).

gericht(gulasch,fleisch).
gericht(wild,fleisch).

gericht(knoedel,knoedel).

gericht(linsen,tagessuppe).
gericht(bohnen,tagessupe).

gericht(pommes,pommes).

gericht(caesar,salat).
gericht(roma,salat).

gericht(currywurst,wurst).
gericht(bratwurst,wurst).

/******************************* STUDENTEN *****************************/
student(stefan).
student(holger).
student(franz).
student(marlene).
student(ricarda).
student(feiernderMensch).

/******************************* GEHT_IN_MENSA *****************************/
geht_in_mensa(stefan).
geht_in_mensa(marlene).
geht_in_mensa(ricarda).

/******************************* SPEISEPLAN *****************************/
mensa('27.11.2020',dorade).
mensa('27.11.2020',caesar).

mensa('30.11.2020',kabeljau).
mensa('30.11.2020',caesar).
mensa('30.11.2020',bohnen).
mensa('30.11.2020',bratwurst).

mensa('01.12.2020',wild).
mensa('01.12.2020',pommes).
mensa('01.12.2020',linsen).
mensa('01.12.2020',currywurst).

mensa('02.12.2020',wild).
mensa('02.12.2020',caesar).

mensa('03.12.2020',gulasch).
mensa('03.12.2020',knoedel).
mensa('03.12.2020',linsen).
mensa('03.12.2020',pommes).
mensa('03.12.2020',roma).

mensa('04.12.2020',lachs).
mensa('04.12.2020',roma).
mensa('04.12.2020',bohnen).

/******************************* SPEISEPLAN AUTO *****************************/
mensa(X,knoedel):- mensa(X,wild).


/******************************* AUFGABEN *****************************/
/*A*/
freitag_always_fisch:- \+ (wochentag(X,freitag), \+ (mensa(X,Y), gericht(Y,fisch))).
/*B*/
wild_always_dumpling:- \+ (mensa(X,wild), \+ mensa(X,knoedel)).
/*C*/
always_soup:- \+ (wochentag(X,_Y), \+ (mensa(X,Y), gericht(Y,tagessuppe))).
/*D*/
always_pommes_salat:- \+ (wochentag(X,_Y), \+ ((mensa(X,Y), gericht(Y,salat));(mensa(X,Y), gericht(Y,pommes)))).
/*E*/
mensahasser(X):- student(X), \+ geht_in_mensa(X).
/*F*/
isst(stefan, X, Y):- mensa(X,Y), gericht(Y,wurst).
stefan_always_wurst:- \+ (mensa(X,Y), gericht(Y,wurst), \+ isst(stefan,X,Y)).


"""

prolog = IsolatedProlog()
prolog.consult(prolog_code, file=False) # Load the prolog facts directly from the string instead of the file

print("Gibt es jeden Freitag Fisch?\n--------------------------------")
print("?- freitag_always_fisch")
query = prolog.query("freitag_always_fisch")
print(" - ", "yes" if (list(query)) else "no")
print("--------------------------------\n")

print("Gibt es immer Knödel zu Wild?\n--------------------------------")
print("?- wild_always_dumpling")
query = prolog.query("wild_always_dumpling")
print(" - ", "yes" if (list(query)) else "no")
print("--------------------------------\n")

print("Gibt es immer Suppe?\n--------------------------------")
print("?- always_soup")
query = prolog.query("always_soup")
print(" - ", "yes" if (list(query)) else "no")
print("--------------------------------\n")

print("Gibt es immer pommes oder salat?\n--------------------------------")
print("?- always_pommes_salat")
query = prolog.query("always_pommes_salat")
print(" - ", "yes" if (list(query)) else "no")
print("--------------------------------\n")

print("Gibt es Mensahasser?\n--------------------------------")
print("?- mensahasser(X)")
query = prolog.query("mensahasser(X)")
print(" - ", "yes" if (list(query)) else "no")
print("--------------------------------\n")

print("Wenn es Wurst gibt, isst Stefan Wurst.\n--------------------------------")
print("?- stefan_always_wurst.")
query = prolog.query("stefan_always_wurst")
print(" - ", "yes" if (list(query)) else "no")
print("--------------------------------\n")


print("ist Stefan Student?\n--------------------------------")
print("?- student(stefan).")
query = prolog.query("student(stefan)")
print(" - ", "yes" if (list(query)) else "no")
print("--------------------------------\n")

"""
#/******************************* NICHT FUNKTIONIERT *****************************/
print("Gibt es jeden Freitag Fisch?\n--------------------------------")
print("?- freitag_always_fisch")
query = prolog.query("\+ (wochentag(X,freitag), \+ (mensa(X,Y), gericht(Y,fisch)))")
print(" - ", "yes" if (list(query)) else "no")
print("--------------------------------\n")
"""
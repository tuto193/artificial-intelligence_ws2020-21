import pyswip as pl
import uuid
import tempfile


class IsolatedProlog(pl.Prolog):
    """
    Simple wrapper around the basic Prolog-class from pyswip for use in
    notebooks. This wrapper uses a dedicated Prolog module for each of it's
    instances, separating them from one another.
    """

    def __init__(self, module=None):
        """
        SimpleProlog constructor.

        Parameters:
        ---
        module: str or None
            The module to connect this instance to. If None (default) a new
            random module is created
        """
        if module is None:
            module = "m" + uuid.uuid4().hex
        self.module_name = str(module)
        self.module = pl.newModule(self.module_name)

    def asserta(self, assertion, catcherrors=False):
        """
        call asserta/1 in the prolog instance
        """
        next(
            self.query(
                assertion.join(["asserta((", "))."]), catcherrors=catcherrors
            )
        )

    def assertz(self, assertion, catcherrors=False):
        """
        call assertz/1 in the prolog instance
        """
        next(
            self.query(
                assertion.join(["assertz((", "))."]), catcherrors=catcherrors
            )
        )

    def dynamic(cls, term, catcherrors=False):
        """
        call dynamic/1 in the prolog instance
        """
        next(
            self.query(
                term.join(["dynamic((", "))."]), catcherrors=catcherrors
            )
        )

    def retract(cls, term, catcherrors=False):
        """
        call retract/1 in the prolog instance
        """
        next(
            self.query(
                term.join(["retract((", "))."]), catcherrors=catcherrors
            )
        )

    def retractall(cls, term, catcherrors=False):
        """
        call retractall/1 in the prolog instance
        """
        next(
            self.query(
                term.join(["retractall((", "))."]), catcherrors=catcherrors
            )
        )

    def consult(self, knowledge_base, file=True, catcherrors=False):
        """
        Load the specified knowledge_base in the prolog interpreter. To
        circumvent a SWI-Prolog limitation, a new temporary file is created on
        every consult.

        Parameters:
        ---
        knowledge_base: str
            The knowledge base to load. This has to be a string containing
            either the filename (default) or the facts to load (if file is
            False). The knowledge base will be written into a temporary file
            before it is loaded into prolog.
        file: bool
            If True (default), the knowledge_base parameter is interpreted as
            a filename. If False the knowledge_base is assumed to contain the
            facts to load.
        catcherrors: bool
            Catch errors that might occur.
        """
        # write all facts into a tempfile first to circumvent the prolog-consult limitation
        if file:
            with open(knowledge_base, "r") as f:
                knowledge_base = f.read()
        with tempfile.NamedTemporaryFile(mode="w", suffix=".pl") as f:
            f.write(knowledge_base)
            f.seek(0)
            next(
                self.query(
                    f.name.join(["consult('", "')"]), catcherrors=catcherrors
                )
            )

    def query(self, query, maxresult=-1, catcherrors=True, normalize=True):
        """
        Run a prolog query and return a python-generator.
        If the query is a yes/no question, returns {} for yes, and nothing for no.
        Otherwise returns a generator of dicts with variables as keys.

        >>> prolog = SimpleProlog()
        >>> prolog.assertz("father(michael,john)")
        >>> prolog.assertz("father(michael,gina)")
        >>> bool(list(prolog.query("father(michael,john)")))
        True
        >>> bool(list(prolog.query("father(michael,olivia)")))
        False
        >>> print sorted(prolog.query("father(michael,X)"))
        [{'X': 'gina'}, {'X': 'john'}]
        """
        return self._QueryWrapper()(
            self.module_name + ":" + query, maxresult, catcherrors, normalize
        )

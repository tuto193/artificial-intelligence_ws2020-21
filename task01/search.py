import Maze from maze
import numpy as np
import time


def bfs(maze, start, goal) -> (list, (int, int, int)):
    """
    Implementation of the breadth first search.

    Returns:
        list: the path from the start to the goal
        (int, int, int): a tuple composed of (in this order):
            * amount of expanded nodes,
            * maximal depth of the search tree, and
            * maximal size of the search front.
    """
    level = [start]
    visited = [start]
    next_level = []
    path:list = []
    expanded_nodes = 0
    max_depth= 0
    max_front_size = 0
    while len(level) > 0:
        path.append(level[-1])
        level.pop()
        if path[-1] == goal:
            path = np.array(path).T
            return path, (expanded_nodes, max_depth, max_front_size)
        neighbors = maze.get_neighbors(path[-1], set(visited))
        visited += neighbors
        next_level += neighbors
        actual_front = len(level) + len(next_level)
        max_front_size = max_front_size if max_front_size > actual_front else actual_front
        expanded_nodes += 1
        if len(level) == 0:
            level = next_level
            next_level = []
            max_depth += 1

m = Maze.load_from_file("examples/labyrinth_20x20.maze")
path, stats = bfs(m, m.start, m.end)
print("naive search length", len(path[0]))
fig = m.plot()
fig.gca().scatter(path[1], path[0], c='lightcoral', zorder=2, alpha=0.8)
plt.show()


def dfs(maze, start , goal) -> (list, (int, int, int)):
    """
    Implementation of the depth first search
    """
